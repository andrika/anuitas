package com.example.hp.anuitas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.hp.anuitas.AnuitasBiasaAkanDatang.AnuitasBiasa_AkanDatang;
import com.example.hp.anuitas.AnuitasBiasaNilaiSekarang.AnuitasBiasa;
import com.example.hp.anuitas.AnuitasDiMukaAkanDatang.AnuitasDiMuka_AkanDatang;
import com.example.hp.anuitas.AnuitasDiMukaNilaiSekarang.AnuitasDiMuka;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void A1 (View view){
        Intent Clicked1 = new Intent (MainActivity.this, AnuitasBiasa.class);
        startActivity(Clicked1);
    }
    public void A3 (View view){
        Intent Clicked2 = new Intent (MainActivity.this, AnuitasDiMuka.class);
        startActivity(Clicked2);
    }
    public void A2 (View view){
        Intent Clicked3 = new Intent (MainActivity.this, AnuitasBiasa_AkanDatang.class);
        startActivity(Clicked3);
    }
    public void A4 (View view){
        Intent Clicked4 = new Intent (MainActivity.this, AnuitasDiMuka_AkanDatang.class);
        startActivity(Clicked4);
    }
}
