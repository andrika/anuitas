package com.example.hp.anuitas.AnuitasDiMukaNilaiSekarang;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.anuitas.R;

import java.text.DecimalFormat;


public class NActivityAnuitasMuka extends Fragment {
    Button btnCalculate;
    Button btnReset;
    private EditText txtFutureValue, txtAnnuity ,txtInterestRate;
    TextView txtViewResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_anuitas_muka_n, container, false);

        btnCalculate = view.findViewById(R.id.btnCalculatePV);
        btnReset = view.findViewById(R.id.btnResetPV);
        txtFutureValue = view.findViewById(R.id.idPresentValue);
        txtInterestRate = view.findViewById(R.id.idInterestRate);
        txtAnnuity = view.findViewById(R.id.idAnnuity);
        txtViewResult =  view.findViewById(R.id.idViewResult);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtFutureValue.getText().toString().trim().length() == 0 ||
                        txtInterestRate.getText().toString().trim().length() == 0 ||
                        txtAnnuity.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Data cannot to be empty!! Fill the blank space", Toast.LENGTH_LONG).show();
                } else {
                    String fillFutureValue = txtFutureValue.getText().toString();
                    String fillAnnuity = txtAnnuity.getText().toString();
                    String fillInterestRate = txtInterestRate.getText().toString();

                    Double txtPresentValue = Double.parseDouble(fillFutureValue);
                    Double txtAnnuity = Double.parseDouble(fillAnnuity);
                    Double txtInterestRate = Double.parseDouble(fillInterestRate);

                    Double countTemp1 = txtPresentValue * txtInterestRate/txtAnnuity;
                    Double countTemp2 = Math.log(1 + txtInterestRate);
                    Double countTemp3 = 1 - countTemp1;
                    Double countTemp4 = Math.log(countTemp3);
                    Double ResultFinal = -(countTemp4/countTemp2);

                    DecimalFormat df = new DecimalFormat("##0.00");

                    txtViewResult.setText(String.valueOf(df.format(Math.ceil(ResultFinal))));
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                txtFutureValue.setText("");
                txtAnnuity.setText("");
                txtInterestRate.setText("");
                txtViewResult.setText(R.string.the_result);
            }
        });

        return view;
    }
}
