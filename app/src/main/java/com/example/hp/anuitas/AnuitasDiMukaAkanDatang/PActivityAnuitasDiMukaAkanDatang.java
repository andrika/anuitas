package com.example.hp.anuitas.AnuitasDiMukaAkanDatang;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.anuitas.R;

import java.text.DecimalFormat;

public class PActivityAnuitasDiMukaAkanDatang extends Fragment {
    Button btnCalculate;
    Button btnReset;
    private EditText txtFutureValue, txtInterestRate, txtPeriod;
    TextView txtViewResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_anuitas_muka_akan_datang_p, container, false);


        btnCalculate = view.findViewById(R.id.btnCalculatePV);

        btnReset = view.findViewById(R.id.btnResetPV);
        txtFutureValue = view.findViewById(R.id.idFutureValue);
        txtInterestRate = view.findViewById(R.id.idInterestRate);
        txtPeriod = view.findViewById(R.id.idPeriod);
        txtViewResult =  view.findViewById(R.id.idViewResult);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtFutureValue.getText().toString().trim().length() == 0 ||
                        txtInterestRate.getText().toString().trim().length() == 0 ||
                        txtPeriod.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Data cannot to be empty!! Fill the blank space", Toast.LENGTH_LONG).show();
                } else {
                    String fillFutureValue = txtFutureValue.getText().toString();
                    String fillInterestRate = txtInterestRate.getText().toString();
                    String fillPeriod = txtPeriod.getText().toString();

                    Double txtFutureValue = Double.parseDouble(fillFutureValue);
                    Double txtInterestRate = Double.parseDouble(fillInterestRate);
                    Double txtPeriod = Double.parseDouble(fillPeriod);

                    Double countTemp1 = Math.pow(1 + txtInterestRate,txtPeriod);
                    Double countTemp2 = countTemp1 - 1;
                    Double countTemp3 = countTemp2/txtInterestRate;
                    Double countTemp4 = 1 + txtInterestRate;
                    Double countTemp5 = countTemp3 * countTemp4;
                    Double ResultFinal = txtFutureValue/countTemp5;

                    DecimalFormat df = new DecimalFormat("##0.00");

                    txtViewResult.setText(String.valueOf(df.format(ResultFinal)));
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                txtFutureValue.setText("");
                txtInterestRate.setText("");
                txtPeriod.setText("");
                txtViewResult.setText(R.string.the_result);
            }
        });
        return view;
    }
}
