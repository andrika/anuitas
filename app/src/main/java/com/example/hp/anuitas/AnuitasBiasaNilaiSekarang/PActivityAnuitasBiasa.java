package com.example.hp.anuitas.AnuitasBiasaNilaiSekarang;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.anuitas.R;

import java.text.DecimalFormat;

import static java.lang.Math.abs;

public class PActivityAnuitasBiasa extends Fragment {
    Button btnCalculate;
    Button btnReset;
    private EditText txtPresentValue, txtInterestRate, txtPeriod;
    TextView txtViewResult;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,@Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_anuitas_biasa_p, container, false);


        btnCalculate = view.findViewById(R.id.btnCalculatePV);

        btnReset = view.findViewById(R.id.btnResetPV);
        txtPresentValue = view.findViewById(R.id.idPresentValue);
        txtInterestRate = view.findViewById(R.id.idInterestRate);
        txtPeriod = view.findViewById(R.id.idPeriod);
        txtViewResult =  view.findViewById(R.id.idViewResult);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPresentValue.getText().toString().trim().length() == 0 ||
                        txtInterestRate.getText().toString().trim().length() == 0 ||
                        txtPeriod.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Data cannot to be empty!! Fill the blank space", Toast.LENGTH_LONG).show();
                } else {
                    String fillPresentValue = txtPresentValue.getText().toString();
                    String fillInterestRate = txtInterestRate.getText().toString();
                    String fillPeriod = txtPeriod.getText().toString();

                    Double txtPresentValue = Double.parseDouble(fillPresentValue);
                    Double txtInterestRate = Double.parseDouble(fillInterestRate);
                    Double txtPeriod = Double.parseDouble(fillPeriod);



                    Double countTemp1 = txtPresentValue * txtInterestRate;
                    Double countTemp2 = Math.pow(1 + txtInterestRate,-txtPeriod);
                    Double countTemp3 = 1 - countTemp2;
                    Double ResultFinal = countTemp1/countTemp3;

                    DecimalFormat df = new DecimalFormat("##0.00");

                    txtViewResult.setText(String.valueOf(df.format(ResultFinal)));
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                txtPresentValue.setText("");
                txtInterestRate.setText("");
                txtPeriod.setText("");
                txtViewResult.setText(R.string.the_result);
            }
        });
        return view;
    }
}
